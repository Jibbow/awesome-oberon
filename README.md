# Installation
```
$ git clone https://gitlab.com/jibbow/awesome-oberon.git
$ mv -bv awesome-oberon/* ~/.config/awesome; rm -rf awesome-oberon
```
Or directly clone this repo into `~/.config/` and rename it to `awesome` so that this file is located at `~/.config/awesome/README.md`.

# Testing your changes in a new virtual X-Session:
You need to have `Xephyr` installed to run a virtual X-Session.
If you already have it installed, you can start it with this command
```
Xephyr :5 & sleep 1 ; DISPLAY=:5 awesome
```
or run `test.sh` from this repo.
