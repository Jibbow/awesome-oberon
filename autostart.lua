-- autostart applications --
local autostart = function(awful)
	awful.spawn("thunderbird", { tag = "mail" })
end

return autostart
