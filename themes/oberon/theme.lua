---------------------------
-- Oberon awesome theme --
---------------------------


local theme     = {}

theme_path      = os.getenv("HOME") .. "/.config/awesome/themes/oberon"

stylefactory = assert(loadfile(theme_path .. "/styling.lua"))()
stylefactory(theme)

theme.icon_dir  = theme_path .. "/icons"
theme.wallpaper = theme_path .. "/wallpaper.png"


-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in your rc.lua


-- ICONS --
theme.awesome_icon                                  = theme.icon_dir .. "/wolf_dark_background.png"
theme.taglist_squares_sel                           = "/usr/share/awesome/themes/default/taglist/squarefw.png"
theme.taglist_squares_unsel                         = "/usr/share/awesome/themes/default/taglist/squarew.png"
theme.menu_submenu_icon                             = "/usr/share/awesome/themes/default/submenu.png"

theme.titlebar_close_button_normal                  = theme.icon_dir .. "/titlebar/close_normal.png"
theme.titlebar_close_button_focus                   = theme.icon_dir .. "/titlebar/close_normal.png"
theme.titlebar_close_button_focus_hover             = theme.icon_dir .. "/titlebar/close_focus.png"
theme.titlebar_close_button_focus_press             = theme.icon_dir .. "/titlebar/close_focus.png"
theme.titlebar_minimize_button_normal               = theme.icon_dir .. "/titlebar/minimize_normal.png"
theme.titlebar_minimize_button_focus                = "/usr/share/awesome/themes/default/titlebar/minimize_focus.png"

theme.titlebar_ontop_button_normal_inactive         = "/usr/share/awesome/themes/default/titlebar/ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive          = "/usr/share/awesome/themes/default/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active           = "/usr/share/awesome/themes/default/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active            = "/usr/share/awesome/themes/default/titlebar/ontop_focus_active.png"

theme.titlebar_sticky_button_normal_inactive        = "/usr/share/awesome/themes/default/titlebar/sticky_normal_inactive.png"
theme.titlebar_sticky_button_focus_inactive         = "/usr/share/awesome/themes/default/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_active          = "/usr/share/awesome/themes/default/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_active           = "/usr/share/awesome/themes/default/titlebar/sticky_focus_active.png"

theme.titlebar_floating_button_normal_inactive      = "/usr/share/awesome/themes/default/titlebar/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive       = "/usr/share/awesome/themes/default/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active        = "/usr/share/awesome/themes/default/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_active         = "/usr/share/awesome/themes/default/titlebar/floating_focus_active.png"

theme.titlebar_maximized_button_normal_inactive     = theme.icon_dir .. "/titlebar/maximize_normal.png"
theme.titlebar_maximized_button_focus_inactive      = theme.icon_dir .. "/titlebar/maximize_normal.png"
theme.titlebar_maximized_button_normal_active       = theme.icon_dir .. "/titlebar/maximize_normal_active.png"
theme.titlebar_maximized_button_focus_active        = theme.icon_dir .. "/titlebar/maximize_normal_active.png"

theme.layout_fairh = "/usr/share/awesome/themes/default/layouts/fairhw.png"
theme.layout_fairv = "/usr/share/awesome/themes/default/layouts/fairvw.png"
theme.layout_floating  = "/usr/share/awesome/themes/default/layouts/floatingw.png"
theme.layout_magnifier = "/usr/share/awesome/themes/default/layouts/magnifierw.png"
theme.layout_max = "/usr/share/awesome/themes/default/layouts/maxw.png"
theme.layout_fullscreen = "/usr/share/awesome/themes/default/layouts/fullscreenw.png"
theme.layout_tilebottom = "/usr/share/awesome/themes/default/layouts/tilebottomw.png"
theme.layout_tileleft   = "/usr/share/awesome/themes/default/layouts/tileleftw.png"
theme.layout_tile = "/usr/share/awesome/themes/default/layouts/tilew.png"
theme.layout_tiletop = "/usr/share/awesome/themes/default/layouts/tiletopw.png"
theme.layout_spiral  = "/usr/share/awesome/themes/default/layouts/spiralw.png"
theme.layout_dwindle = "/usr/share/awesome/themes/default/layouts/dwindlew.png"
theme.layout_cornernw = "/usr/share/awesome/themes/default/layouts/cornernww.png"
theme.layout_cornerne = "/usr/share/awesome/themes/default/layouts/cornernew.png"
theme.layout_cornersw = "/usr/share/awesome/themes/default/layouts/cornersww.png"
theme.layout_cornerse = "/usr/share/awesome/themes/default/layouts/cornersew.png"


-- Define the icon theme for application icons. If not set then the icons
-- from /usr/share/icons and /usr/share/icons/hicolor will be used.
theme.icon_theme = nil

return theme

-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:textwidth=80
